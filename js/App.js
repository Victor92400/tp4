$ (
    function() {

        $("label").hover(function () {
            $(this).find("span").css("display", "inline");
        },
        function(){$(this).find("span").css("display", "none");}) 

        let result, total2;
        let total3 = 0;

        $('input[name=type]').click(function() {
            $('input[name=type]:checked').each(function() { 
                result = parseInt($(this).data('price'));
                $("div.stick-right > div.tile > p").text(result + " €");
            });
        });
        $('input[name=pate]').click(function() {
            $('input[name=pate]:checked').each(function() { 
                total2 = parseInt($(this).data('price'));
                $("div.stick-right > div.tile > p").text(result + total2 + " €");
            });
        });
        $('input[name=extra]').click(function() {
            if ($('input[name=extra]:checked')) { 
                total3 = total3 + parseInt($(this).data('price'));
                $("div.stick-right > div.tile > p").text(result + total2 + total3 + " €");
            };
        });

        $("div.nb-parts > input").on('input',function(){
            let part = +$("div.nb-parts > input").val();
            $(".temporaire").remove();
            $(".pizza-pict").removeClass().addClass("pizza-pict").addClass("pizza-" + part).addClass("first");
            let reste, nbpizzas, i;
            if(part > 6) {
                reste = part%6;
                nbpizzas = (part - reste)/6;
                if(reste!=0) {
                    $(".pizza-pict").removeClass().addClass("pizza-pict").addClass("pizza-" + reste).addClass("first");
                }
                for(i=0;i<nbpizzas;i++) {
                    $(".first").before($('<span></span>').addClass('pizza-pict').addClass('pizza-6').addClass("temporaire"));
                }
            }
           });

           $(".next-step").click(function() {
               $("div .no-display").css("display", "inline");
               $(".next-step").addClass("no-display");
           })
           $(".add").click(function() {
               $(".add").before($('<input></input>').attr({type: 'text'}).css("display", "block"));
           })
           $(".done").click(function() {
               let prenom = $("div.infos-client > div.type:first > input").val();
               $("div.typography-row").empty();
               let remerciement = $("<h1>Merci "+ prenom + " !" + "</h1>").css({'text-align': 'center', 'margin-top': '1%'}).addClass("remerciement");
               $("div.typography-row").after().html(remerciement);
               $(".remerciement").after($("<h4>Votre commande sera livrée dans 15 minutes</h4>").css({'text-align': 'center', 'margin-top': '2%'}));
           })
    }
)
